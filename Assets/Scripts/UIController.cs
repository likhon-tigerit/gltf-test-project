﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GLTFast;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//test link
//https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/Duck/glTF/Duck.gltf
//C:/Users/USER/Desktop/_projects/gltf-test-project/Assets/StreamingAssets/Duck.gltf

public class UIController : MonoBehaviour
{
    public GltfAsset gltfAsset;
    public Button loadButton;
    //public Button resetScene;
    public InputField inputField;

    public Dropdown m_Dropdown;
    public string m_Value;

    void Start()
    {
        //Fetch the Dropdown GameObject
        //m_Dropdown = GetComponent<Dropdown>();
        //Add listener for when the value of the Dropdown changes, to take action
        m_Dropdown.onValueChanged.AddListener(delegate {
            DropdownValueChanged(m_Dropdown);
        });

        //Initialise the Text to say the first value of the Dropdown
        m_Value = m_Dropdown.options[m_Dropdown.value].text;
    }

    //Ouput the new value of the Dropdown into Text
    void DropdownValueChanged(Dropdown change)
    {
        m_Value = m_Dropdown.options[m_Dropdown.value].text;
    }


    public void DropdownLoadButtonClick()
    {
        gltfAsset.LoadClick(m_Value);
    }


    public void LoadButtonClick() {
        gltfAsset.LoadClick(inputField.text);
    }

    public void SceneRestart() {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        gltfAsset.ClearScenes();
    }
}
